$(function () {
    initSoldierChart();
    initPeoInCamp();
    initCategoryInCamp();
});
/*初始化士兵在营变化趋势图表*/
function initSoldierChart() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('soldier'));

    // 指定图表的配置项和数据
    var option = {
        color:['#fdd275'],
        xAxis: {
            type: 'category',
            axisLine:{
                lineStyle:{
                    color:'#18295d'
                }
            },
            axisTick:{
                show: false
            },
            axisLabel: {
                color: '#7597fa'
            },
            data: ['07-01', '07-05', '07-10', '07-15', '07-20', '07-25', '07-30']
        },
        yAxis: {
            type: 'value',
            min: 0,
            max: 100,
            axisLine: {
                lineStyle: {
                    color:'#18295d'
                }
            },
            axisTick:{
                show: false
            },
            axisLabel: {
                color: '#7597fa',
                formatter:'{value}%'
            },
            splitLine: {
                show:false
            }
        },
        series: [{
            type: 'line',
            itemStyle: {
                opacity: 0
            },
            lineStyle: {
                width: 5
            },
            areaStyle: {
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2:0,
                    y2:0.8,
                    colorStops: [{
                        offset: 0, color: 'rgba(239,184,64,1)' // 0% 处的颜色
                    }, {
                        offset: 0.5, color: 'rgba(239,184,64,.5)' // 50% 处的颜色
                    },{
                        offset: 1, color: 'rgba(239,184,64,.1)' // 100% 处的颜色
                    }],
                    global: false // 缺省为 false
                }
            },
            smooth: true,
            data: [20, 45, 60, 80, 50, 30, 70]
        }]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
/*初始化在营人数圆环图*/
function initPeoInCamp() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('peopleInCamp'));
    var placeHolderStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: false
            },
            color: "rgba(0,0,0,0)",
            borderWidth: 0
        },
        emphasis: {
            color: "rgba(0,0,0,0)",
            borderWidth: 0
        }
    };
    var dataStyle = {
        formatter: '{c}',
        position: 'center',
        fontSize: '50',
        color: '#fff'
    };
    var totalNum = 5000;  // 总人数
    var inCampNum = 2500; // 在营人数
    var outCampNUm = totalNum - inCampNum;  // 不在营人数
    // 指定图表的配置项和数据
    var option = {
        title:{
            text: '在营人数',
            left: '50%',
            top: '60%',
            textAlign: 'center',
            textStyle: {
                fontWeight: 'normal',
                textAlign: 'center',
                fontSize: '28',
                color: '#a1c4f5'
            }
        },
        series: [
            //下层环形配置
            {
                type: 'pie',
                hoverAnimation: false, //鼠标经过的特效
                radius: ['65%', '80%'],
                startAngle: 220,
                label: {
                    position: 'center'
                },
                data: [{
                    value: totalNum,
                    itemStyle: {
                        color: '#1d3578'
                    }
                }],
                roundCap:1,
                silent: true
            },
            //上层环形配置
            {
                type: 'pie',
                hoverAnimation: false, //鼠标经过的特效
                radius: ['65%', '80%'],
                startAngle: 220,
                itemStyle:{
                    color:(params) => {
                        if(params.value >= 0 && params.value <= totalNum/2){
                            return "#76f0de"
                        }else{
                            return "#ecb15c"
                        }
                    }
                },
                label: {
                    position: 'center'
                },
                data: [{
                    value: inCampNum,
                    label: dataStyle,
                }, {
                    value: outCampNUm,
                    itemStyle: placeHolderStyle,
                }],
                roundCap:1,
                silent: true
            }
        ]
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
/*初始化在营人员分类圆环图*/
function initCategoryInCamp() {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('categoryInCamp'));
    var data = [
        {
            name: '在营士兵',
            value: 3846,
            totalNum: 4500
        },{
            name: '在营干部',
            value: 365,
            totalNum: 500
        },{
            name: '在营家属',
            value: 924,
            totalNum: 1500
        },{
            name: '在营访客',
            value: 97,
            totalNum: 200
        }]

    var titleArr= [];
    var seriesArr=[];
    data.forEach(function(item, index){
        titleArr.push(
            {
                text:item.name,
                left: index * 20 + 19+'%',
                bottom: 0,
                textAlign: 'center',
                textStyle: {
                    fontWeight: 'normal',
                    fontSize: '20',
                    color: '#a1c4f5',
                    textAlign: 'center'
                }
            }
        );
        seriesArr.push(
            {
                name: item.name,
                type: 'pie',
                clockWise: false,
                startAngle: 180,
                radius: [40, 48],
                itemStyle:  {
                    color: (params) => {
                        if(params.value >= 0 && params.value <= item.totalNum/2){
                            return "#32feee"
                        }else{
                            return "#f6e37f"
                        }
                    }
                },
                hoverAnimation: false,
                center: [index * 20 + 20 +'%', '50%'],
                data: [{
                    value: item.totalNum-item.value,
                    itemStyle: {
                        color: '#1e367d'
                    },
                    labelLine: {
                        show: false
                    }
                },{
                    value: item.value,
                    label: {
                        formatter: function(params){
                            return params.value ;
                        },
                        position: 'center',
                        show: true,
                        fontSize: '20',
                        color: '#a1c4f5'
                    },
                }],
                roundCap: 1,
                silent: true
            }
        )
    });

    // 指定图表的配置项和数据
    var option =  {
        title:titleArr,
        series: seriesArr
    };

    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
}
