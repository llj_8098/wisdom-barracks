$(function () {
  initSoldierChart();
  initVehicleTrends();
  initVehicleAccessTime();
  initCarInCamp();
  initCategoryInCamp();
});
/*初始化当日新兵训练车辆使用情况图表*/
function initSoldierChart() {
  // 基于准备好的dom，初始化echarts实例
  var myChart = echarts.init(document.getElementById('soldier'));

  // 指定图表的配置项和数据
  var option = {
    color: ['#9d50fa','#4c61fc','#ffa956','#c0ff5c','#73ff80'],
    legend: {
      left: 'center',
      bottom: '10%',
      textStyle: {
        color: '#fff'
      }
    },
    series: [
      {
        name: '当日新兵训练车辆使用情况',
        type: 'pie',
        radius: [20, 70],
        roseType: 'area',
        center: ['50%','40%'],
        label: {
          color: '#fff',
          formatter: function (params) {
            return params.data.value + '（' + params.data.total + '）'
          }
        },
        data: [
          {value: 20, total:100,name: '炊事车'},
          {value: 7, total:90,name: '运输车'},
          {value: 8, total:40,name: '通信车'},
          {value: 15, total:80,name: '救护车'},
          {value: 56, total:206,name: '指挥车'}
        ]
      }
    ]

  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
}
/*初始化当日车辆进出动向*/
function initVehicleTrends() {
  // 基于准备好的dom，初始化echarts实例
  var myChart = echarts.init(document.getElementById('vehicleTrends'));

  // 指定图表的配置项和数据
  var option = {
    legend: {
      right: 0,
      textStyle: {
        color: '#fff'
      },
      orient: 'vertical'
    },
    dataset: {
      dimensions: ['product', '出营', '进营'],
      source: [
        {product: '北京', '出营': 43.3, '进营': 85.8},
        {product: '广东', '出营': 83.1, '进营': 73.4},
        {product: '新疆', '出营': 86.4, '进营': 65.2},
        {product: '西藏', '出营': 72.4, '进营': 53.9},
        {product: '内蒙古', '出营': 72.4, '进营': 53.9}
      ]
    },
    xAxis: {
      type: 'category',
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        color: '#fff'
      }
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisLabel: {
        color: '#7597fa'
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      }
    },
    series: [
      {
        type: 'bar',
        barWidth: 15,
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(81,224,241,1)'
        }, {
          offset: 1,
          color: 'rgba(81,224,241,.5)'
        }])
      },
      {
        type: 'bar',
        barWidth: 15,
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(238,216,122,1)'
        }, {
          offset: 1,
          color: 'rgba(238,216,122,.1)'
        }])
      }
    ]
  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
}
/*初始化分时段车辆进出情况*/
function initVehicleAccessTime() {
// 基于准备好的dom，初始化echarts实例
  var myChart = echarts.init(document.getElementById('vehicleAccessTime'));

  // 指定图表的配置项和数据
  var option = {
    grid: {
      left: 100
    },
    color: ['#f8df7c','#d4a6fe'],
    legend: {
      show: true,
      right: 0,
      textStyle: {
        color: '#7dabef'
      },
      icon: 'roundRect'
    },
    yAxis: {
      type: 'category',
      axisLine: {
        show: false
      },
      axisLabel: {
        color: '#7597fa'
      },
      axisTick: {
        show: false
      },
      data: ['20:00-22:00', '18:00-20:00', '16:00-18:00', '12:00-14:00', '10:00-12:00', '08:00-10:00', '06:00-08:00']
    },
    xAxis: {
      type: 'value',
      axisLine: {
        show: false
      },
      axisLabel: {
        color: '#7597fa'
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      }

    },
    series: [
      {
        name: '进营',
        type: 'line',
        data: [0,17,15,24,22,28,32,36,40,44,24,50]
      },
      {
        name: '出营',
        type: 'line',
        data: [0,7,1,14,2,8,32,3,20,30,39,45]
      }
    ]
  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
}
/*初始化当前车辆圆环图*/
function initCarInCamp() {
  // 基于准备好的dom，初始化echarts实例
  var myChart = echarts.init(document.getElementById('vehicleInCamp'));
  var placeHolderStyle = {
    normal: {
      label: {
        show: false
      },
      labelLine: {
        show: false
      },
      color: "rgba(0,0,0,0)",
      borderWidth: 0
    },
    emphasis: {
      color: "rgba(0,0,0,0)",
      borderWidth: 0
    }
  };
  var dataStyle = {
    formatter: '{c}',
    position: 'center',
    fontSize: '40',
    color: '#fff'
  };
  var totalNum = 5000;  // 车辆总数
  var inCampNum = 2500; // 当前车辆
  var outCampNUm = totalNum - inCampNum;  // 不在营车辆
  // 指定图表的配置项和数据
  var option = {
    title:{
      text: '当前车辆',
      left: '50%',
      top: '60%',
      textAlign: 'center',
      textStyle: {
        fontWeight: 'normal',
        textAlign: 'center',
        fontSize: '20',
        color: '#a1c4f5'
      }
    },
    series: [
      //下层环形配置
      {
        type: 'pie',
        hoverAnimation: false, //鼠标经过的特效
        radius: ['65%', '80%'],
        startAngle: 220,
        label: {
          position: 'center'
        },
        data: [{
          value: totalNum,
          itemStyle: {
            color: '#1d3578'
          }
        }],
        roundCap:1,
        silent: true
      },
      //上层环形配置
      {
        type: 'pie',
        hoverAnimation: false, //鼠标经过的特效
        radius: ['65%', '80%'],
        startAngle: 220,
        itemStyle:{
          color:(params) => {
            if(params.value >= 0 && params.value <= totalNum/2){
              return "#76f0de"
            }else{
              return "#ecb15c"
            }
          }
        },
        label: {
          position: 'center'
        },
        data: [{
          value: inCampNum,
          label: dataStyle,
        }, {
          value: outCampNUm,
          itemStyle: placeHolderStyle,
        }],
        roundCap:1,
        silent: true
      }
    ]
  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
}
/*初始化车辆分类圆环图*/
function initCategoryInCamp() {
  // 基于准备好的dom，初始化echarts实例
  var myChart = echarts.init(document.getElementById('categoryInCamp'));
  var data = [
    {
      name: '指挥车',
      value: 3846,
      totalNum: 4500
    },{
      name: '救护车',
      value: 365,
      totalNum: 500
    },{
      name: '通信车',
      value: 924,
      totalNum: 1500
    },{
      name: '运输车',
      value: 97,
      totalNum: 200
    },{
      name: '炊事车',
      value: 97,
      totalNum: 200
    }]

  var titleArr= [];
  var seriesArr=[];
  data.forEach(function(item, index){
    titleArr.push(
      {
        text:item.name +'\n\n'+ item.totalNum,
        left: index * 20 + 9+'%',
        bottom: 0,
        textAlign: 'center',
        textStyle: {
          fontWeight: 'normal',
          fontSize: '18',
          color: '#a1c4f5',
          textAlign: 'center'
        }
      }
    );
    seriesArr.push(
      {
        name: item.name,
        type: 'pie',
        clockWise: false,
        startAngle: 180,
        radius: [40, 48],
        itemStyle:  {
          color: (params) => {
            if(params.value >= 0 && params.value <= item.totalNum/2){
              return "#32feee"
            }else{
              return "#f6e37f"
            }
          }
        },
        hoverAnimation: false,
        center: [index * 20 + 10 +'%', '50%'],
        data: [{
          value: item.totalNum-item.value,
          itemStyle: {
            color: '#1e367d'
          },
          labelLine: {
            show: false
          }
        },{
          value: item.value,
          label: {
            formatter: function(params){
              return params.value ;
            },
            position: 'center',
            show: true,
            fontSize: '20',
            color: '#a1c4f5'
          },
        }],
        roundCap: 1,
        silent: true
      }
    )
  });

  // 指定图表的配置项和数据
  var option =  {
    title:titleArr,
    series: seriesArr
  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
}
